#! /usr/bin/env bash

cp config/.bashrc ~
source ~/.bashrc

if [ ! -d ~/.config ]; then
  mkdir ~/.config
fi
cp config/.config/lxkeymap.cfg ~/.config/

cp -r bin ~

cp -r minecraft ~

sudo apt-get --purge remove wolfram-engine wolframscript

sudo apt-get update
sudo apt-get dist-upgrade

## PERSO
sudo apt-get install zim htop vlc youtube-dl

sudo apt-get clean

## BIOINFO
#sudo apt-get install zim freemind jupyter-notebook jupyter-nbextension-jupyter-js-widgets
#
#pip3 install --user bash_kernel
#python3 -m bash_kernel.install --user
